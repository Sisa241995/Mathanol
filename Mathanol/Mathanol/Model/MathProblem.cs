﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mathanol.Model.Term;

namespace Mathanol.Model
{
    public class MathProblem : BaseModel
    {
        private Operation op;
       
        private static readonly Random _rand = new Random();
        public string Problem
        {
            get; set;
        }

        public int Answer1 { get; set; }
        public int Answer2 { get; set; }
        public int Answer3 { get; set; }
        public int Answer4 { get; set; }

        public int Result
        {
            get; set; 
        }
        public MathProblem(int level)
        {
            op = Operation.GetNonAtomicRandom((int) Math.Sqrt(2 * level) + 15);
            Result = op.GetResult();
            Problem = op.GetStringRepresentation();
            var list = op.GetFakeResults();
            list.RemoveAll(x => x == Result);
            list = RemoveDuplicates(list);

            var bigger = list.Where(x => x > Result).ToList();
            var smaller = list.Where(y => y < Result).ToList();

            var listOfLists = new List<List<int>>
            {
                bigger,
                smaller,
                list
            };

            var listToUse = listOfLists[_rand.Next(0, 3)];
            while (listToUse.Count < 3)
            {
                listToUse.Add(list[_rand.Next(0, list.Count)]);
                listToUse = RemoveDuplicates(listToUse);
            } 

            while (listToUse.Count > 3)
            {
                listToUse.RemoveAt(_rand.Next(0, listToUse.Count));
            }

            listToUse.Add(Result);

            var randomValue = _rand.Next(0, listToUse.Count);
            Answer1 = listToUse[randomValue];
            listToUse.RemoveAt(randomValue);
            randomValue = _rand.Next(0, listToUse.Count);
            Answer2 = listToUse[randomValue];
            listToUse.RemoveAt(randomValue);
            randomValue = _rand.Next(0, listToUse.Count);
            Answer3 = listToUse[randomValue];
            listToUse.RemoveAt(randomValue);
            Answer4 = listToUse[0];
        }

        private static List<int> RemoveDuplicates(List<int> list)
        {
            var temp = new List<int>();
            while (list.Count > 0)
            {
                int x = list[0];
                temp.Add(x);
                list.RemoveAll(y => x == y);
            }
            return temp;
        }

    }
}
