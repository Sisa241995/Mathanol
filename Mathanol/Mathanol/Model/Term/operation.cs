﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathanol.Model.Term
{
    public abstract class Operation
    {
        protected int Level;
        protected static readonly Random Rand = new Random();
        private const int _atomicLimit = 2;
        private const int _sumLimit = 3;
        private const int _multLimit = 6;
        private const int _rootLimit = 7;
        private const int _squareLimit = 8;

        protected Operation(int level)
        {
            Level = level;
        }

        public abstract string GetStringRepresentation();
        public abstract string GetOperationSign();

        public abstract int GetPrecedence();

        public abstract int GetResult();

        public abstract List<int> GetFakeResults();

        public static Operation GetRandom(int level)
        {
            if (level == 0) return new AtomicOperation(1, false);
            var result = Rand.Next(1, level);
            return result <= _atomicLimit ? new AtomicOperation(level, false) : GetNonAtomicRandom(level);

        }

        public static Operation GetNonAtomicRandom(int level)
        {
            if (level == 0) return new Sum(1);
            var result = Rand.Next(1, 9);
            if (result <= _sumLimit)
            {
                return new Sum(level / 2);
            }
            else if (result <= _multLimit)
            {
                return new Multiplication(level / 3);
            }
            else if (result <= _rootLimit)
            {
                return new SquareRoot(level);
            }
            /*else if (result <= _squareLimit)
            {
                return new Square(level);
            }*/
            else
            {
                return new Sum(1);
            }
        }
    }


}
