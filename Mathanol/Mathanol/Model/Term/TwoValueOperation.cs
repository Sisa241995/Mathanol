﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathanol.Model.Term
{
    public abstract class TwoValueOperation : Operation
    {
        protected Operation Left;
        protected Operation Right;
        protected TwoValueOperation(int level) : base(level)
        {
            
        }
        public override string GetStringRepresentation()
        {
            var left = Left.GetStringRepresentation();
            var right = Right.GetStringRepresentation();
            if (Left.GetPrecedence() < GetPrecedence()) left = "(" + left + ")";
            if (Right.GetPrecedence() < GetPrecedence()) right = "(" + right + ")";
            return left + " " + GetOperationSign() + " " + right;
        }
    }
}
