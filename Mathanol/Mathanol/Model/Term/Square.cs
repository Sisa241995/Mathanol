﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathanol.Model.Term
{
    public class Square : OneValueOperation
    {
        public Square(int level) : base(level)
        {
            Value = Rand.Next(level * 3 / 4, level);
        }

        public override string GetOperationSign()
        {
            return "%s\u00B2";
        }

        public override int GetResult()
        {
            return (int) Math.Pow(Value, 2);
        }

        public override List<int> GetFakeResults()
        {
            return new List<int>
            {
                (int) Math.Pow(Value - 1, 2),
                GetResult(),
                (int) Math.Pow(Value + 1, 2)
            };
        }
    }
}
