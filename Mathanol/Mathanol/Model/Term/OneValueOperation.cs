﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathanol.Model.Term
{
    public abstract class OneValueOperation : Operation
    {
        protected int Value;
        protected OneValueOperation(int level) : base(level)
        {
            
        }
        public override string GetStringRepresentation()
        {
            return GetOperationSign().Replace("%s", Value.ToString());
        }
        public override int GetPrecedence()
        {
            return int.MaxValue;
        }
    }
}
