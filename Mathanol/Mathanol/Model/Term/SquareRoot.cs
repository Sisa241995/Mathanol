﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathanol.Model.Term
{
    public class SquareRoot : OneValueOperation
    {
        public SquareRoot(int level) : base(level)
        {
            Value = (int) Math.Pow(Rand.Next(level * 3 / 4, level), 2);
        }
        public override string GetOperationSign()
        {
            return "\u221A(%s)";
        }
        public override int GetPrecedence()
        {
            return 4;
        }
        public override int GetResult()
        {
            return (int) Math.Sqrt(Value);
        }
        public override List<int> GetFakeResults()
        {
            return new List<int>
            {
                GetResult() + 2,
                GetResult() + 1,
                GetResult(),
                GetResult() - 1
            };
        }
        
    }
}
