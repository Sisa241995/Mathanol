﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathanol.Model.Term
{
    class Multiplication : TwoValueOperation
    {
        private const int _factor = 2;
        public Multiplication(int level) : base(level)
        {
            Left = GetRandom(level);
            Right = GetRandom(level);
        }

        public override string GetOperationSign()
        {
            return "*";
        }

        public override int GetPrecedence()
        {
            return 2;
        }

        public override int GetResult()
        {
            return Left.GetResult() * Right.GetResult();
        }

        public override List<int> GetFakeResults()
        {
            var leftList = Left.GetFakeResults();
            var rightList = Right.GetFakeResults();
            return (from x in leftList from y in rightList select x * y).ToList();
        }
    }
}
