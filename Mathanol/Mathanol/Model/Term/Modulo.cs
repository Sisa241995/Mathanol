﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathanol.Model.Term
{
    public class Modulo : TwoValueOperation
    {
        public Modulo(int level) : base(level)
        {
            Left = GetRandom(level);
            Right = new AtomicOperation(level, true);
        }

        public override string GetOperationSign()
        {
            return "%";
        }

        public override int GetPrecedence()
        {
            return 3;
        }

        public override int GetResult()
        {
            return Left.GetResult() % Right.GetResult();
        }

        public override List<int> GetFakeResults()
        {
            var result = GetResult();
            var leftList = Left.GetFakeResults();
            var rightList = Right.GetFakeResults();
            return (from x in leftList from y in rightList select x % y).ToList();
        }
    }
}
