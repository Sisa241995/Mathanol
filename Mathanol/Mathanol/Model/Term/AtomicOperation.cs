﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathanol.Model.Term
{
    public class AtomicOperation : OneValueOperation
    {
        private const int _limit = 8;
        private const int _factor = 2;
        public AtomicOperation(int level, bool positive) : base(level)
        {
            var left = positive ? 1 : -_limit - level * _factor;
            Value = Rand.Next(left, _limit + level * _factor);
        }
        public override string GetStringRepresentation()
        {
            return Value.ToString();
        }
        public override string GetOperationSign()
        {
            return "%s";
        }
        public override int GetResult()
        {
            return Value;
        }
        public override List<int> GetFakeResults()
        {
            return new List<int>
            {
                Value + 1, 
                Value,
                Value - 1
            };
        }
    }
}
