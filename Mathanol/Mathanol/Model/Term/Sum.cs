﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathanol.Model.Term
{
    public class Sum : TwoValueOperation
    {
        private static readonly int[] _fakeValues = new int[] { -10, 10, -4, 4 , 0};
        public Sum(int level) : base(level)
        {
            Left = GetRandom(level);
            Right = GetRandom(level);
        }
        public override string GetOperationSign()
        {
            return "+";
        }
        public override string GetStringRepresentation()
        {
            if (Right is AtomicOperation && Right.GetResult() < 0)
            {
                return Left.GetStringRepresentation() + " - " + (int) Math.Abs(Right.GetResult());
            }
            else
            {
                return base.GetStringRepresentation();
            }
        }
        public override int GetPrecedence()
        {
            return 1;
        }
        public override int GetResult()
        {
            return Left.GetResult() + Right.GetResult();
        }
        public override List<int> GetFakeResults()
        {
            var leftList = Left.GetFakeResults();
            var rightList = Right.GetFakeResults();
            var resultsWithoutHelp = (from x in leftList from y in rightList select x + y).ToList();
            return (from x in resultsWithoutHelp from y in _fakeValues select x+y).ToList();

        }
    }
}
