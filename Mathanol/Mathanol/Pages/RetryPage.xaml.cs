﻿using Mathanol.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Mathanol.Pages
{
    public partial class RetryPage : ContentPage
    {
        public RetryPage(int Number)
        {
            InitializeComponent();
            BindingContext = new RetryViewModel(Number);
        }
    }
}
