﻿using Mathanol.Interfaces;
using Mathanol.Model;
using Mathanol.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Phone.Devices.Notification;
using Xamarin.Forms;

namespace Mathanol.ViewModel
{
    class GameViewModel : BaseViewModel
    {
        private int correctAnswers;
        private Timer timer;
        private int timeLeft;
        private Random rand;
        private MathProblem problem;
        private int panicTime;
        private int panicCount = 0;

        public MathProblem Problem
        {
            get
            {
                return problem;
            }
            set
            {
                if (value != problem)
                {
                    problem = value;
                    OnPropertyChanged("Problem");
                }
            }
        }

        public ICommand AnswerCommand
        {
            get;
            set;
        }

        public GameViewModel()
        {
            correctAnswers = 0;
            Problem = new MathProblem(correctAnswers);
            rand = new Random();
            AnswerCommand = new Command<Button>(answer);
            timeLeft = createRandomTime(10000, 190000);
            panicTime = rand.Next(0, timeLeft / 4);
            timer = new Timer(decreaseTime, null, 0, 1000);
        }

        private void answer(Button button)
        {
            if (button.Text == Problem.Result.ToString())
            {
                var col = button.BackgroundColor;
                button.BackgroundColor = Color.Green;
                if (Device.OS == TargetPlatform.Android) DependencyService.Get<IAudio>().playSound(true);
                Task.Run(async () =>
                {
                    await Task.Delay(100);
                    button.BackgroundColor = col;
                });
                correctAnswers++;
                Problem = new MathProblem(correctAnswers);
            } else
            {
                var col = button.BackgroundColor;
                button.BackgroundColor = Color.Red;
                if (Device.OS == TargetPlatform.Android) DependencyService.Get<IAudio>().playSound(false);
                Task.Run(async () =>
                {
                    await Task.Delay(100);
                    button.BackgroundColor = col;
                });
                timeLeft -= 20000;
                if (timeLeft <= 0) endGame();
                else Problem = new MathProblem(correctAnswers);
            }
            
        }

        public void decreaseTime(Object stateInfo)
        {
            timeLeft -= 1000;
            if (timeLeft <= 0 ) endGame();
            if (timeLeft <= panicTime) panic();
        }

        private void panic()
        {
           if(panicCount == 0)
            {
                vibrate(250);
            }
            panicCount++;
            panicCount = panicCount % 2; 
        }

        public int createRandomTime(int min, int max)
        {
            double u1 = rand.NextDouble(); //these are uniform(0,1) random doubles
            double u2 = rand.NextDouble();
            double randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Sin(2.0 * Math.PI * u2); //random normal(0,1)
            int randNormal = (max - min)/2 + (int)(40000.0 * randStdNormal);
            if (randNormal < min || randNormal > max)
            {
                System.Diagnostics.Debug.WriteLine("Ignoring time " + randNormal);
                return createRandomTime(min, max);
            }
            System.Diagnostics.Debug.WriteLine("Using time " + randNormal);
            return randNormal;
        }

        public void endGame()
        {
            timer.Dispose();
            vibrate(1000);
            Device.BeginInvokeOnMainThread(() =>
            {
                App.navigateTo(new RetryPage(correctAnswers));
            });
        }

        public void vibrate(int millis)
        {
            if (Device.OS == TargetPlatform.Android) DependencyService.Get<IVibration>().Vibrate(millis);
            if (Device.OS == TargetPlatform.WinPhone) VibrationDevice.GetDefault().Vibrate(TimeSpan.FromMilliseconds(millis));
        }
    }
}
