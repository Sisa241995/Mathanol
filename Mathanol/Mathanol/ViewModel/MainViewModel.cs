﻿using Mathanol.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Mathanol.ViewModel
{
    class MainViewModel : BaseViewModel
    {
        public ICommand NavigateToGamePageCommand
        {
            get;
            set;
        }

       public MainViewModel()
        {
            NavigateToGamePageCommand = new Command(o => App.navigateTo(new GamePage()));
        }

    }
}
