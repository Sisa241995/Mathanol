﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mathanol.ViewModel
{
    class RetryViewModel : MainViewModel
    {
        public int Number
        {
            get;
            private set;
        }

        public RetryViewModel(int Number)
        {
            this.Number = Number;
        }
    }
}
