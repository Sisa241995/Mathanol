﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Mathanol
{
    public partial class App : Application
    {
        private static App Instance;

        public App()
        {
            InitializeComponent();

            Instance = this;

            if (Device.OS == TargetPlatform.Android) MainPage = new Mathanol.Pages.MainPage();
            else MainPage = new NavigationPage(new Mathanol.Pages.MainPage());
        }

        public static void navigateTo(ContentPage page)
        {
            Instance?.MainPage.Navigation.PushModalAsync(page);
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
