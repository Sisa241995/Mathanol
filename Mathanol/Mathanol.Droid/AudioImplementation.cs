using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Media;
using System.Runtime.CompilerServices;
using Mathanol.Interfaces;
using Mathanol.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(AudioImplementation))]
namespace Mathanol.Droid
{
    public class AudioImplementation : IAudio
    {
        public AudioImplementation() { }

        private MediaPlayer _mediaPlayer;

        public void playSound(bool right)
        {
            _mediaPlayer = MediaPlayer.Create(global::Android.App.Application.Context, right ? Resource.Raw.right : Resource.Raw.wrong);
            _mediaPlayer.Start();
        }
    }
}