using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Mathanol.Interfaces;
using Mathanol.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(VibrationImplementation))]
namespace Mathanol.Droid
{
    class VibrationImplementation : IVibration
    {
        public void Vibrate(int time)
        {
            Vibrator vib = (Vibrator) MainActivity.app.GetSystemService(Context.VibratorService);
            vib.Vibrate(time);
        }
    }
}